cc=gcc
server=server
client=client
cflags=-Wall -Wextra

all : ${server} ${client}

${client} : ${client}.c
	${cc} ${cflags} -o $@ $<

${server} : ${server}.c
	${cc} ${cflags} -o $@ $<

clean : 
	rm -f ${server} ${client}
