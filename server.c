#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/time.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#define MAX_CLIENTS 12

struct Client
{
    char* name;
    int socketfd;
};

void startServer(uint16_t port)
{
    const int NAME_LENGTH = 30;
    const int BUFFER_SIZE = 1024;
    const char NAME_PREFIX = '@';
    const char* CLIENTS_COMMAND = "/clients";

    struct Client clients[MAX_CLIENTS];

    for (int i = 0; i < MAX_CLIENTS; i++)
    {
        clients[i].socketfd = 0;
        clients[i].name = NULL;
    }

    int listenSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (listenSocket == -1)
        exit(EXIT_FAILURE);

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    bind(listenSocket, (struct sockaddr *) &address, sizeof(address));

    listen(listenSocket, MAX_CLIENTS);

    fd_set readfds;
    int maxfds = listenSocket;
    char buffer[BUFFER_SIZE];

    while (1)
    {
        // add server listening socket to watchlist
        FD_ZERO(&readfds);
        FD_SET(listenSocket, &readfds);

        // add clients sockets to watchlist
        for (int i = 0; i < MAX_CLIENTS; i++)
        {
            int clientSocket = clients[i].socketfd;

            if (clientSocket > 0)
                FD_SET(clientSocket, &readfds);

            if (clientSocket > maxfds)
                maxfds = clientSocket;
        }

        select(maxfds + 1, &readfds, NULL, NULL, NULL);

        // new client connected to server
        if (FD_ISSET(listenSocket, &readfds))
        {
            int clientSocket = accept(listenSocket, NULL, NULL);

            char rawName[NAME_LENGTH];

            // blocks until name has been recived
            ssize_t readSize = read(clientSocket, rawName, sizeof(rawName));

            char* name = (char*)calloc((readSize), sizeof(char));
            strncpy(name, rawName, readSize);
            // telnet hack
            if (name[readSize - 1] == '\n')
            {
                name[readSize - 1] = '\0';
                name[readSize - 2] = '\0';
            }

            // add client to list
            for (int i = 0; i < MAX_CLIENTS; i++)
            {
                if (clients[i].socketfd == 0)
                {
                    clients[i].name = name;
                    clients[i].socketfd = clientSocket;
                    break;
                }
            }
        }
        // message from existing client
        else
        {
            for (int i = 0; i < MAX_CLIENTS; i++)
            {
                int clientSocket = clients[i].socketfd;

                if (FD_ISSET(clientSocket, &readfds))
                {
                    bzero(&buffer, sizeof(buffer));
                    ssize_t readSize = read(clientSocket, buffer, sizeof(buffer));
                    if (readSize > 0)
                    {
                        // send message to specified client
                        if (buffer[0] == NAME_PREFIX)
                        {
                            // get client name and message from buffer
                            char* msg;
                            char* name = strtok_r(buffer, " ", &msg);
                            name++; // get rid of @ from begining of name

                            int found = 0;
                            for (int j = 0; j < MAX_CLIENTS; j++)
                            {
                                if (clients[j].socketfd > 0 && strcmp(clients[j].name, name) == 0)
                                {
                                    char* response = (char*)malloc(sizeof(clients[i].name) + sizeof(msg));
                                    sprintf(response, "%s: %s", clients[i].name, msg);

                                    write(clients[j].socketfd, response, strlen(response));

                                    free(response);
                                    found = 1;
                                }
                            }

                            if (found == 0)
                            {
                                char* response = "Client not found\n";
                                write(clients[i].socketfd, response, strlen(response));
                            }
                        }
                        // send list of clients
                        else if (strncmp(buffer, CLIENTS_COMMAND, strlen(CLIENTS_COMMAND)) == 0)
                        {
                            char* response = (char*)malloc(NAME_LENGTH * (MAX_CLIENTS + 1) * sizeof(char));

                            sprintf(response, "Client list: ");
                            for (int j = 0; j < MAX_CLIENTS; j++)
                            {
                                if (clients[j].socketfd > 0)
                                {
                                    if (j == i)
                                        sprintf(response, "%s %s(you),", response, clients[j].name);
                                    else
                                        sprintf(response, "%s %s,", response, clients[j].name);
                                }
                            }
                            sprintf(response, "%s\n", response);

                            write(clients[i].socketfd, response, strlen(response));

                            free(response);
                        }
                        // send message to all clients
                        else
                        {
                            char* response = (char*)malloc(sizeof(clients[i].name) + readSize);
                            sprintf(response, "%s: %s", clients[i].name, buffer);

                            for (int j = 0; j < MAX_CLIENTS; j++)
                            {
                                if (clients[j].socketfd > 0 && j != i)
                                    write(clients[j].socketfd, response, strlen(response));
                            }
                            free(response);
                        }
                    }
                    else if (readSize == 0)
                    {
                        clients[i].socketfd = 0;
                        clients[i].name = NULL;
                        close(clientSocket);
                    }
                }
            }
        }
    }

    close(listenSocket);
}

int getPids(int* pids, char* pName)
{
    static const char* PROC_DIR = "/proc";
    static const char* FILE_NAME = "comm";
    static const int PATH_LENGTH = 50;
    static const int NAME_SIZE = 50;

    DIR* dir;
    struct dirent* entry;

    dir = opendir(PROC_DIR);
    if (!dir)
    {
        fprintf(stderr, "Could not open directory %s!\n", PROC_DIR);
        exit(EXIT_FAILURE);
    }

    int i = 0;
    while ((entry = readdir(dir)))
    {
        if (isdigit(*entry->d_name))
        {
            char path[PATH_LENGTH];
            snprintf(path, sizeof(path), "%s/%s/%s", PROC_DIR, entry->d_name, FILE_NAME);
            int file = open(path, O_RDONLY);
            if (file == -1)
            {
                fprintf(stderr, "Could not open file %s!\n", FILE_NAME);
                exit(EXIT_FAILURE);
            }

            char name[NAME_SIZE];
            ssize_t size = read(file, &name, sizeof(name));
            name[size - 1] = '\0';
            if (strcmp(name, pName) == 0)
                pids[i++] = atoi(entry->d_name);

            close(file);
        }
    }

    closedir(dir);

    return i;
}

void stopServer(char* name)
{
    int pids[50];
    name = name + 2;
    int size = getPids(pids, name);

    for (int i = 0; i < size; i++)
    {
        kill(pids[i], SIGINT);
    }
}

void setupDaemon()
{
    pid_t pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);
    else if (pid > 0)
        exit(EXIT_SUCCESS);

    if (setsid() < 0)
        exit(EXIT_FAILURE);

    signal(SIGCHLD, SIG_IGN);

    pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);
    else if (pid > 0)
        exit(EXIT_SUCCESS);

    umask(0);
    chdir("/");

    for (int fd = sysconf(_SC_OPEN_MAX); fd >= 0; fd--)
    {
        close(fd);
    }

    stdin = fopen("/dev/null", "r");
    stdout = fopen("/dev/null", "w+");
    stderr = fopen("/dev/null", "w+");
}

int main(int argc, char* argv[])
{
    int port = 0;

    int option;
    while ((option = getopt(argc, argv, "p:q")) != -1)
    {
        switch (option)
        {
        case 'p':
            port = atoi(optarg);
            break;
        case 'q':
            stopServer(argv[0]);
            return EXIT_SUCCESS;
        case '?':
            fprintf(stderr, "Usage: [-p port] [-q]\n");
        default:
            return EXIT_FAILURE;
        }
    }

    if (port == 0)
    {
        fprintf(stderr, "Requires at least one of options: [-p port] [-q]\n");
        return EXIT_FAILURE;
    }

    //setupDaemon();
    daemon(0, 0);
    startServer((uint16_t)port);

    return EXIT_SUCCESS;
}
