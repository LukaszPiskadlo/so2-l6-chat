#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>

void startClient(char* address, int port, char* name)
{
    const int BUFFER_SIZE = 1024;

    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket == -1)
    {
        fprintf(stderr, "Could not open socket\n");
        exit(EXIT_FAILURE);
    }

    struct hostent* host;
    host = gethostbyname(address);

    struct sockaddr_in sockAddress;
    sockAddress.sin_family = AF_INET;
    sockAddress.sin_port = htons((uint16_t)port);
    memcpy(&sockAddress.sin_addr.s_addr, host->h_addr, host->h_length);

    int conn = connect(clientSocket, (struct sockaddr*) &sockAddress, sizeof(sockAddress));
    if (conn == -1)
    {
        fprintf(stderr, "Connection refused\n");
        exit(EXIT_FAILURE);
    }

    // send name first
    write(clientSocket, name, strlen(name));

    char buffer[BUFFER_SIZE];
    fd_set readfds;

    printf("Type /clients for list of clients\n");
    printf("Type @name to send private message to name\n");
    printf("Type /q to quit\n");

    while (1)
    {
        FD_ZERO(&readfds);
        FD_SET(clientSocket, &readfds);
        FD_SET(STDIN_FILENO, &readfds);

        select(clientSocket + 1, &readfds, NULL, NULL, NULL);

        if (FD_ISSET(clientSocket, &readfds))
        {
            bzero(&buffer, sizeof(buffer));
            ssize_t readSize = read(clientSocket, buffer, sizeof(buffer));
            if (readSize > 0)
                printf("%s", buffer);
        }

        if (FD_ISSET(STDIN_FILENO, &readfds))
        {
            bzero(&buffer, sizeof(buffer));
            fgets(buffer, BUFFER_SIZE, stdin);
            if (buffer[0] == '/' && buffer[1] == 'q')
                break;

            write(clientSocket, buffer, strlen(buffer));
        }
    }

    close(clientSocket);
}

int main(int argc, char* argv[])
{
    int port = 0;
    char* address = NULL;
    char* name = NULL;

    int option;
    while ((option = getopt(argc, argv, "a:p:n:")) != -1)
    {
        switch (option)
        {
        case 'a':
            address = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'n':
            name = optarg;
            break;
        case '?':
            fprintf(stderr, "Usage: [-a address] [-p port] [-n name]\n");
        default:
            return EXIT_FAILURE;
        }
    }

    if (address == NULL || port == 0 || name == NULL)
    {
        fprintf(stderr, "Requires all options: [-a address] [-p port] [-n name]\n");
        return EXIT_FAILURE;
    }

    startClient(address, port, name);

    return EXIT_SUCCESS;
}
